
(function ($) {

Drupal.behaviors.AudioPrompter = {
  attach: function (context, settings) {

    $("#audioprompter-saveButton", context).click(function() {
      var applet = $("#audioprompter-applet").get(0);
      applet.sendGongRequest('StopMedia', 'audio');

      // Save the file
      var nid = $('[name=nid]').val();

      // Tell the applet to post the voice recording to the backend PHP code
      var path = Drupal.settings.basePath +"audioprompter/receive/"+nid;
      var sessioncookie = $('[name=sess]').val()+'='+$('[name=cook]').val();
      var fid = applet.sendGongRequest("PostToForm", path, "voicefile", sessioncookie, "temp");

      if (fid == null || fid == ""){
        // TODO: drupal set message error
        alert("save failed!");
        // $("#edit-field-"+field_name+"-"+langcode+"-"+delta+"-ajax-wrapper .form-managed-file").prepend('<div class="messages error file-upload-js-error">Failed to submit the voice recording!</div>');
      }
      else{
        applet.sendGongRequest("ClearMedia", 'audio');
        var language = $('[name=language]').val();
        $.ajax({
          type: 'GET',
          url: Drupal.settings.basePath +"audioprompter/next/"+language+"/js",
          dataType: 'json',
          success: function (ret) {
            $('#audioprompter-prompt').html(ret.prompt);
            $('[name=nid]').val(ret.nid);
            if (ret.nid == 0) {
              $('#audioprompter-controls').hide();
            }
            else {
              $('#audioprompter-controls').removeClass('recorded').removeClass('recording').addClass('prompt-ready');
            }
          }
        });
      }
    });

    $(".audioprompter-recordButton").click(function() {
      var applet = $("#audioprompter-applet").get(0);
      applet.sendGongRequest('StopMedia', 'audio');
      applet.sendGongRequest('RecordMedia', 'audio');
      setTimeout( function() {
        $('#audioprompter-controls').removeClass('prompt-ready').removeClass('recorded').addClass('recording');
      }, 100);
    });

    $("#audioprompter-replayButton").click(function() {
      var applet = $("#audioprompter-applet").get(0);
      applet.sendGongRequest('StopMedia', 'audio');
      applet.sendGongRequest('PlayMedia', 'audio');
    });

    $("#audioprompter-stopButton").click(function() {
      $('#audioprompter-controls').removeClass('recording');
      var applet = $("#audioprompter-applet").get(0);
      setTimeout(function() {
        applet.sendGongRequest('StopMedia', 'audio');
        $('#audioprompter-controls').addClass('recorded');
        applet.sendGongRequest('PlayMedia', 'audio');
      }, 300);
    });

  }
}

}(jQuery));