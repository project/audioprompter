<?php

function audioprompter_settings() {

  // TODO: change this to a select with a list of filefields.
  $form['audioprompter_filefield'] = array(
    '#type' => 'textfield',
    '#title' => t('Audio file field'),
    '#description' => t('The machine name of the filefield into which audio files should be saved.'),
    '#default_value' => variable_get('audioprompter_filefield', 'field_audio'),
  );
  // TODO: change this to a select with a list of fields.
  $form['audioprompter_promptfield'] = array(
    '#type' => 'textfield',
    '#title' => t('Audio prompt field'),
    '#description' => t('The machine name of the field to display as the prompt for audio input.'),
    '#default_value' => variable_get('audioprompter_promptfield', 'field_body'),
  );

  $form['audioprompter_conversion'] = array(
    '#type' => 'select',
    '#title' => t('Audio conversion'),
    '#description' => t('If you have installed <a href="http://sox.sourceforge.net">sox</a>, audioprompter can use it to convert the recorded audio to another format.  Neither of the native formats are playable by many browsers or media players.'),
    '#default_value' => variable_get('audioprompter_conversion', 'ima'),
    '#options' => array(
      'spx' => t('Speex (native)'),
      'ima' => t('Compressed .wav (native)'),
      'wav' => t('Uncompressed .wav'),
      'wavpcm' => t('Uncompressed PCM .wav'),
      'ogg' => t('ogg vorbis'),
    ),
  );
  $output = $status = '';
  exec('sox -h', $output, $status);
  if ($status) {
    $form['audioprompter_conversion']['#disabled'] = TRUE;
  }

  return system_settings_form($form);
}